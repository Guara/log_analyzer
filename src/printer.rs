//! Module that contains structure useful to print info in a structured manner

use tabled::Tabled;

#[derive(Tabled)]
#[tabled(rename_all = "PascalCase")]
pub struct LogInfo {
    #[tabled(rename = "Type")]
    name: String,
    count: usize,
    bytes: usize,
}

impl LogInfo {
    pub fn new(name: String, bytes: usize) -> Self {
        Self {
            name,
            bytes,
            count: 1,
        }
    }

    pub fn add(&mut self, bytes: usize) {
        self.count += 1;
        self.bytes += bytes;
    }
}
