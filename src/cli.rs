//! CLI arguments parser

use clap::Parser;

/// Parse cli arguments in a convenient struct
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
pub(crate) struct Args {
    /// Name of the person to greet
    #[clap(short, long)]
    file: String,
}

impl Args {
    pub(crate) fn file(&self) -> &str {
        self.file.as_str()
    }
}
