//! A log analyzer tool
//!
//! Reads a file where each line is an arbitrary json object, the only needed field is `type`.
//! It output a table containing the number of objects with each type, and the total byte size of
//! all the messages with each type.

#![deny(unsafe_code)]
#![warn(future_incompatible, missing_docs)]

use std::collections::HashMap;

use anyhow::{Context, Result};
use clap::Parser;
use tracing::info;

use tabled::TableIteratorExt;

#[cfg(feature = "tokio")]
use crate::async_reader::AsyncReaderBuilder;
use crate::cli::Args;
use crate::deserialize::Log;
use crate::printer::LogInfo;
#[cfg(not(feature = "tokio"))]
use crate::reader::ReaderBuilder;

#[cfg(feature = "tokio")]
mod async_reader;
mod cli;
mod deserialize;
mod printer;
#[cfg(not(feature = "tokio"))]
mod reader;

fn main() -> Result<()> {
    let args = Args::parse();
    tracing_subscriber::fmt::init();
    run(args)
}

#[cfg(feature = "tokio")]
fn run(args: Args) -> Result<()> {
    tokio::runtime::Builder::new_current_thread()
        .build()?
        .block_on(log_analyzer(args.file()))
}

#[cfg(not(feature = "tokio"))]
fn run(args: Args) -> Result<()> {
    log_analyzer(args.file())
}

#[cfg(not(feature = "tokio"))]
fn log_analyzer(filepath: &str) -> Result<()> {
    let reader = ReaderBuilder::new(filepath)
        .with_buffer_size(4096)
        .build()?;
    let mut lines = reader.lines();
    // using `LogInfo` as value will avoid an iteration to transform the data
    let mut log_map: HashMap<String, LogInfo> = HashMap::new();
    while let Some(line) = lines
        .next()
        .transpose()
        .context("Fail to read a line from file")?
    {
        store_info(&mut log_map, line)?;
    }
    print(&mut log_map);
    Ok(())
}

#[cfg(feature = "tokio")]
async fn log_analyzer(filepath: &str) -> Result<()> {
    let reader = AsyncReaderBuilder::new(filepath)
        .with_buffer_size(4096)
        .build()
        .await?;
    let mut lines = reader.lines();
    // using `LogInfo` as value will avoid an iteration to transform the data
    let mut log_map: HashMap<String, LogInfo> = HashMap::new();
    while let Some(line) = lines
        .next_line()
        .await
        .context("Fail to read a line from file")?
    {
        store_info(&mut log_map, line)?;
    }
    print(&mut log_map);
    Ok(())
}

/// Print `LogInfo` as a table on stdout
fn print(log_map: &mut HashMap<String, LogInfo>) {
    info!("Print table:");
    println!("{}", log_map.values().table());
}

/// Store infos about the line in the provided mutable hashmap
fn store_info(log_map: &mut HashMap<String, LogInfo>, line: String) -> Result<()> {
    let bytes = line.len();
    let log: Log =
        serde_json::from_str(line.as_str()).context("Fail to parse a line as a json object")?;
    let log_type = log.name().to_owned();
    info!(size = bytes, r#type = log_type, "Read line");
    if let Some(entries) = log_map.get_mut(log_type.as_str()) {
        entries.add(bytes);
    } else {
        log_map.insert(log_type.clone(), LogInfo::new(log_type, bytes));
    };
    Ok(())
}
