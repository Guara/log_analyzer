//! Module used to store deserialized structures
use serde::de::Visitor;
use serde::{Deserialize, Deserializer};
use std::fmt;
use std::fmt::{Error, Formatter};
use std::ops::Deref;

struct UpperCaseString(String);

impl UpperCaseString {
    pub fn new(value: impl Into<String>) -> Self {
        let string = value.into();
        Self(string.to_uppercase())
    }
}

impl Deref for UpperCaseString {
    type Target = String;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl fmt::Display for UpperCaseString {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl From<UpperCaseString> for String {
    fn from(s: UpperCaseString) -> Self {
        s.0
    }
}

struct UpperCaseStringVisitor;

impl<'de> Visitor<'de> for UpperCaseStringVisitor {
    type Value = UpperCaseString;

    fn expecting(&self, formatter: &mut Formatter) -> Result<(), Error> {
        formatter.write_str("a string")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        Ok(UpperCaseString::new(v))
    }
}

impl<'de> Deserialize<'de> for UpperCaseString {
    fn deserialize<D>(deserializer: D) -> Result<UpperCaseString, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_string(UpperCaseStringVisitor)
    }
}

#[derive(Deserialize)]
pub struct Log {
    #[serde(rename = "type")]
    name: UpperCaseString,
}

impl Log {
    /// Returns the type of the log, in this simple case could consume self to avoid a later allocation
    pub fn name(&self) -> &str {
        &self.name
    }
}
