//! Classic blocking file reader

use std::fs::File;
use std::io::{BufRead, BufReader, Lines};

use anyhow::Result;

use tracing::info;

pub struct ReaderBuilder<'a> {
    filepath: &'a str,
    buffer_capacity: Option<usize>,
}

impl<'a> ReaderBuilder<'a> {
    pub fn new(filepath: &'a str) -> Self {
        Self {
            filepath,
            buffer_capacity: None,
        }
    }

    pub fn with_buffer_size(self, buffer_capacity: usize) -> Self {
        Self {
            filepath: self.filepath,
            buffer_capacity: Some(buffer_capacity),
        }
    }

    pub fn build(self) -> Result<Reader> {
        info!(path = self.filepath, "Open file");
        let file = File::open(self.filepath)?;
        let inner_reader = if let Some(capacity) = self.buffer_capacity {
            BufReader::with_capacity(capacity, file)
        } else {
            BufReader::new(file)
        };

        Ok(Reader { inner_reader })
    }
}

pub struct Reader {
    inner_reader: BufReader<File>,
}

impl Reader {
    pub fn lines(self) -> Lines<BufReader<File>> {
        self.inner_reader.lines()
    }
}
