//! Tokio based asynchronous file reader

use tokio::fs::File;
use tokio::io::{AsyncBufReadExt, BufReader, Lines};

use anyhow::Result;
use tracing::info;

pub struct AsyncReaderBuilder<'a> {
    filepath: &'a str,
    buffer_capacity: Option<usize>,
}

impl<'a> AsyncReaderBuilder<'a> {
    pub fn new(filepath: &'a str) -> Self {
        Self {
            filepath,
            buffer_capacity: None,
        }
    }

    pub fn with_buffer_size(self, buffer_capacity: usize) -> Self {
        Self {
            filepath: self.filepath,
            buffer_capacity: Some(buffer_capacity),
        }
    }

    pub async fn build(self) -> Result<AsyncReader> {
        info!(path = self.filepath, "Open file");
        let file = File::open(self.filepath).await?;
        let inner_reader = if let Some(capacity) = self.buffer_capacity {
            BufReader::with_capacity(capacity, file)
        } else {
            BufReader::new(file)
        };

        Ok(AsyncReader { inner_reader })
    }
}

pub struct AsyncReader {
    inner_reader: BufReader<File>,
}

impl AsyncReader {
    pub fn lines(self) -> Lines<BufReader<File>> {
        self.inner_reader.lines()
    }
}
