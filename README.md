### Exercise binary that reads a file where each line is an arbitrary json object, the only needed field is `type`. 
### It output a table containing the number of objects with each type, and the total byte size of all the messages with each type.

```bash
$ log_analyzer -f ./example.txt
```
